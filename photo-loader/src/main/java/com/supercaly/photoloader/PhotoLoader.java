package com.supercaly.photoloader;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.supercaly.photoloader.Adapter.PhotoAdapter;
import com.supercaly.photoloader.Interfaces.OnAddClickListener;
import com.supercaly.photoloader.Layout.AddButton;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by lorenzo on 24/12/17.
 */

public class PhotoLoader extends Fragment implements OnAddClickListener {
    private static final String TAG = "PhotoLoader";
    public static final String PHOTO_LIST_PARCELABLE = "photo_list";
    public static final String MAX_PHOTO_NUM_PARCELABLE = "max_photo_num";
    public static final String FOLDER_NAME_PARCELABLE = "folder_name";
    public static final int DEFAULT_MAX_PHOTO_NUM = 5;

    //Variabili
    private int maxPhotoNum = DEFAULT_MAX_PHOTO_NUM;
    private String cameraFolderName = TAG;
    private ImagePicker.Builder builder;
    //Widget
    private AddButton mAddBtn;
    private List<Uri> mPhotoList;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private PhotoAdapter mAdapter;


    /**
     * Ritorna una nuova istanza del frammento
     * @return
     */
    public static PhotoLoader newInstance(){
        return new PhotoLoader().setMaxPhotoNum(DEFAULT_MAX_PHOTO_NUM).setFolderName(TAG);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: creo gli elementi principali del frammento");
        mAddBtn = new AddButton(getActivity());
        mAddBtn.setOnAddClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.photo_loader_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = getActivity().findViewById(R.id.photo_list_recyclerview);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        builder = getDefaultBilder();

        if (savedInstanceState != null){
            Log.d(TAG, "onViewCreated: resume "+savedInstanceState);
            mPhotoList = savedInstanceState.getParcelableArrayList(PHOTO_LIST_PARCELABLE);
            maxPhotoNum = savedInstanceState.getInt(MAX_PHOTO_NUM_PARCELABLE);
            cameraFolderName = savedInstanceState.getString(FOLDER_NAME_PARCELABLE);
        }
        else {
            Log.d(TAG, "onViewCreated: create new");
            mPhotoList = new ArrayList<>();
        }
        mAdapter = new PhotoAdapter(mPhotoList, mAddBtn, getActivity(), maxPhotoNum);
        mRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        ArrayList<Uri> photolist = (ArrayList<Uri>) mAdapter.getPhotoList();
        outState.putParcelableArrayList(PHOTO_LIST_PARCELABLE, photolist);
        outState.putInt(MAX_PHOTO_NUM_PARCELABLE, maxPhotoNum);
        outState.putString(FOLDER_NAME_PARCELABLE, cameraFolderName);
        Log.d(TAG, "onSaveInstanceState: "+outState);
        super.onSaveInstanceState(outState);
    }

    /**
     * Ritorna la lista di foto selezionate
     * @return
     */
    public List<Uri> getPhotos(){ return mAdapter.getPhotoList(); }

    /**
     * Metodo per verificare se ho scelto nessuna foto
     * @return
     */
    public boolean isEmpty(){
        return getPhotos().isEmpty();
    }

    /**
     * Getter e Setter per l'errore
     * @param error
     */
    public void setError(@Nullable String error){
        mAddBtn.setError(error);
    }
    public String getError(){
        return mAddBtn.getError();
    }

    /**
     * Getter e Setter per il numero massimo di foto selezionabili
     */
    public PhotoLoader setMaxPhotoNum(int max){
        this.maxPhotoNum = max;
        return this;
    }
    public int getMaxPhotoNum(){
        return maxPhotoNum;
    }

    /**
     * Getter e Setter per il drowable del bottone nello stato normale e di errore
     * @return
     */
    public int getDrawableNormal() {
        return mAddBtn.getDrawableNormal();
    }

    public int getDrawableError() {
        return mAddBtn.getDrawableError();
    }

    public PhotoLoader setDrawableNormal(int drawableNormal) {
        this.mAddBtn.setDrawableNormal(drawableNormal);
        return this;
    }

    public PhotoLoader setDrawableError(int drawableError) {
        this.mAddBtn.setDrawableError(drawableError);
        return this;
    }

    public PhotoLoader setFolderName(String name){
        this.cameraFolderName = name;
        return this;
    }

    public String getFolderName(){
        return cameraFolderName;
    }

    public ImagePicker.Builder getBuilder() {
        return builder;
    }

    /**
     * Gestisco i risultati dell'image picker
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: "+requestCode);
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            List<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            for (Image i : images) {
                Uri uri = Uri.parse("file://"+i.getPath());
                Log.d(TAG, "onActivityResult: "+uri, new Exception());
                mAdapter.addItem(uri);
            }
            Log.v(TAG, "onActivityResult: All photos loaded!");
        }
    }


    @Override
    public void onClick() {
        Log.v(TAG, "Avvio il gallery picker");
        startPickUpActivity();
    }

    /**
     * Avvio la picker activity
     */
    private void startPickUpActivity() {
        int currentMaxPhotoNum = maxPhotoNum - mAdapter.getPhotoList().size();
        Log.d(TAG, "startPickUpActivity: max foto num: "+currentMaxPhotoNum);

        builder.setMaxSize(currentMaxPhotoNum)                     //  Max images can be selected
                .setSavePath(cameraFolderName)         //  Image capture folder name
                .start();
    }

    public ImagePicker.Builder getDefaultBilder() {
        return builder = ImagePicker.with(this)
                .setToolbarColor("#212121")         //  Toolbar color
                .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
                .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
                .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
                .setProgressBarColor("#4CAF50")     //  ProgressBar color
                .setBackgroundColor("#212121")      //  Background color
                .setCameraOnly(false)               //  Camera mode
                .setMultipleMode(true)              //  Select multiple images or single image
                .setFolderMode(true)                //  Folder mode
                .setShowCamera(true)                //  Show camera button
                .setFolderTitle(getString(R.string.albums))           //  Folder title (works with FolderMode = true)
                .setImageTitle(getString(R.string.galleries))         //  Image title (works with FolderMode = false)
                .setDoneTitle(getString(R.string.done))               //  Done button title
                .setLimitMessage(getString(R.string.max_limit_reached))    // Selection limit message
                .setKeepScreenOn(true);              //  Keep screen on when selecting images
    }
}

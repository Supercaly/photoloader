package com.supercaly.photoloader.Layout;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.supercaly.photoloader.Interfaces.OnAddClickListener;
import com.supercaly.photoloader.R;

/**
 * Created by lorenzo on 26/10/17.
 */

public class AddButton extends RelativeLayout {
    private static final String TAG = "AddButton";
    private static final int DEFAULT_DRAWABLE_ERROR = R.drawable.ic_image_add_button_error;
    private static final int DEFAULT_DRAWABLE_NORMAL = R.drawable.ic_image_add_button;

    private OnAddClickListener mAddClickListener;
    private Context mContext;
    private ImageView mImageView;
    private String mErrorMsg;
    private int drawableNormal = DEFAULT_DRAWABLE_NORMAL;
    private int drawableError = DEFAULT_DRAWABLE_ERROR;

    public AddButton(Context context) {
        super(context);
        init(context);
    }

    public AddButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        View mView = LayoutInflater.from(context).inflate(R.layout.add_button_layout, this);
        mImageView = mView.findViewById(R.id.immagine);
        mImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: ");
                mAddClickListener.onClick();
            }
        });
        setError(null);
    }

    public void setOnAddClickListener(OnAddClickListener listener){
        mAddClickListener = listener;
    }

    public void setError(@Nullable String errorMsg){
        if (errorMsg == null){
            removeError();
        }
        else {
            addError(errorMsg);
        }
    }

    private void addError(String error) {
        Log.d(TAG, "addError: Setto il layout d'errore");
        mErrorMsg = error;
        mImageView.setImageDrawable(mContext.getDrawable(drawableError));
    }

    private void removeError() {
        Log.d(TAG, "removeError: Setto il layout normale");
        mErrorMsg = "";
        mImageView.setImageDrawable(mContext.getDrawable(drawableNormal));
    }

    public String getError() {
        return mErrorMsg;
    }

    public int getDrawableNormal() {
        return drawableNormal;
    }

    public int getDrawableError() {
        return drawableError;
    }

    public void setDrawableNormal(int drawableNormal) {
        this.drawableNormal = drawableNormal;
    }

    public void setDrawableError(int drawableError) {
        this.drawableError = drawableError;
    }
}

package com.supercaly.photoloader.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.supercaly.photoloader.Layout.AddButton;
import com.supercaly.photoloader.R;

import java.util.List;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * Created by lorenzo on 26/12/17.
 */
class ItemViewHolder extends RecyclerView.ViewHolder{
    public ImageView immagine;
    public ImageView rimuovi;

    public ItemViewHolder(View itemView) {
        super(itemView);
        immagine = itemView.findViewById(R.id.immagine);
        rimuovi = itemView.findViewById(R.id.rimuovi);
    }
}
class FooterViewHolder extends RecyclerView.ViewHolder{

    public FooterViewHolder(View itemView) {
        super(itemView);
    }
}

public class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "PhotoAdapter";
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;

    private int maxPhotoNum;
    private List<Uri> mPhotoList;
    private AddButton mAddButton;
    private Context mContext;

    public PhotoAdapter(List<Uri> mPhotoList, AddButton mAddButton, Context mContext, int maxPhotoNum) {
        this.mPhotoList = mPhotoList;
        this.mAddButton = mAddButton;
        this.mContext = mContext;
        this.maxPhotoNum = maxPhotoNum;
        Log.d(TAG, "PhotoAdapter: const"+mPhotoList);
        notifyDataSetChanged();
    }

    public void addItem(Uri item){
        mPhotoList.add(item);
        Log.d(TAG, "addItem: aggiungo un elemento in posizione: "+(mPhotoList.size()-1));
        if (mPhotoList.size() >= maxPhotoNum)
            mAddButton.setVisibility(View.GONE);
        notifyDataSetChanged();
    }

    public void removeItem(int position){
        mPhotoList.remove(position);
        Log.d(TAG, "removeItem: rimuovo l'elemento: "+position);
        if (mAddButton.getVisibility() == View.GONE && mPhotoList.size() < maxPhotoNum)
            mAddButton.setVisibility(View.VISIBLE);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.removable_imageview_layout, parent, false);
            return new ItemViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            return new FooterViewHolder(mAddButton);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder){
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            Picasso.with(mContext).load(mPhotoList.get(position))
                    .transform(new RoundedCornersTransformation(15, 0))
                    .centerCrop()
                    .fit()
                    .into(itemViewHolder.immagine);
            itemViewHolder.rimuovi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeItem(position);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1)
            return TYPE_FOOTER;
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mPhotoList.size() + 1;
    }

    public List<Uri> getPhotoList(){
        return mPhotoList;
    }
}

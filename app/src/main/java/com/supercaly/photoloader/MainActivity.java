package com.supercaly.photoloader;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private PhotoLoader mPhotoLoader;
    private TextView mResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null){
            mPhotoLoader = PhotoLoader.newInstance();
            mPhotoLoader.setMaxPhotoNum(10)
                    .setFolderName("PhotoLoaderDemo");
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.layout_aggiungi_immagini, mPhotoLoader, "photo_loader")
                    .commit();
        }else {
            mPhotoLoader = (PhotoLoader) getSupportFragmentManager().findFragmentByTag("photo_loader");
        }

        mResult = findViewById(R.id.result_array);
        Button getResults = findViewById(R.id.result_btn);
        getResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPhotoLoader.isEmpty()){
                    mPhotoLoader.setError("No items");
                    mResult.setText(mPhotoLoader.getError());
                } else {
                    mPhotoLoader.setError(null);
                    List<Uri> uris = mPhotoLoader.getPhotos();
                    StringBuilder builder = new StringBuilder();
                    builder.append("Max photo: "+mPhotoLoader.getMaxPhotoNum());
                    for (Uri u: uris){
                        builder.append(u.getPath()+"\n");
                    }
                    mResult.setText(builder);
                }
            }
        });
    }
}

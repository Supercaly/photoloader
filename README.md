# PhotoLoader
An android library to allow the user to choose photos from the device or camera and show them in the horizontal list <br/>
<img src="https://raw.githubusercontent.com/Supercaly/PhotoLoader/master/device-2018-03-01-111050.png" width="400">
<img src="https://raw.githubusercontent.com/Supercaly/PhotoLoader/master/device-2018-03-01-111123.png" width="400">
## Installation
To use this library use the code above 
#### Gradle
```java
implementation 'com.supercaly:photo-loader:1.1.0'
implementation 'com.github.nguyenhoanglam:ImagePicker:1.2.1' //
implementation 'com.squareup.picasso:picasso:2.5.2'				 //  Check library documentation for newer versions
implementation 'jp.wasabeef:picasso-transformations:2.1.2'		 //
```
#### Maven

```xml
<dependency>
  <groupId>com.supercaly</groupId>
  <artifactId>photo-loader</artifactId>
  <version>1.1.0</version>
  <type>pom</type>
</dependency>
```

## Usage
In your code you only have to create a new instance of the Fragment PhotoLoader then pass it to the FragmentManager
```java
PhotoLoader photoloader = PhotoLoader.newInstance();
getSupportFragmentManager().beginTransaction()
                    .replace(R.id.layout_aggiungi_immagini, photoloader, "photo_loader")
                    .commit();
```
In the main .xml file add a "FrameLayout"
```xml
<FrameLayout
        android:id="@+id/layout_aggiungi_immagini"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginEnd="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        android:orientation="horizontal"/>
```
#### Get the photo list
To get the selected images use
```java
List<Uri> photos = photoloader.getPhotos();
```
that return a list of Uri objects

#### Set the error
It's possible to set an error to notify the user if ther is no photo selected

```java
photoloader.setError("ErrorMsg");
photoloader.setError(null); //the error will no longer be shown
String err = photoloader.getError();
```

#### There are selected photos?
To know at any time if you have selected photos use
```java
boolean isEmpty = photoloader.isEmpty();
```

## Customization
When we create a new instance of the class some values are set by default; we can customize:
* Max photo number
```java
photoloader.setMaxPhotoNum(10);
int max = photoloader.getMaxPhotoNum();
```
* Image Picker Builder
```java
photoloader.getBuilder();
```
* Folder name for camera photos
```java
photoloader.setFolderName("foldername");
```
* Button Drawable in normal state
```java
photoloader.setDrawableNormal(resId);
int resId = photoloader.getDrawableNormal();
```
* Button Drawable in error state
```java
photoloader.setDrawableError(resId);
int resId = photoloader.getDrawableError();
```
## Thanks
The image picking part is made possible by the ***Image Picker*** library by nguyenhoanglam.
Find it at https://github.com/nguyenhoanglam/ImagePicker

All the miages are rendered by the ***Picasso*** library by Square http://square.github.io/picasso/ <br/>
and the circle effect are given by the ***Picasso Transformation*** library by wasabeef https://github.com/wasabeef/picasso-transformations
## Apps that use PhotoLoader
Please send a pull request if you would like to be added here.
| Icon | App name |
| --- | --- |
| No icon | No app |
| No icon | No app |
## License
```
Copyright 2018 Lorenzo Calisti.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
